export enum LocalEnvEnum {
  RABBITMQ_URI = 'RABBITMQ_URI',
  RABBITMQ_USER = 'RABBITMQ_USER',
  RABBITMQ_PASS = 'RABBITMQ_PASS',
  RABBITMQ_EXCHANGE_QUEUE = 'RABBITMQ_EXCHANGE_QUEUE',
  RABBITMQ_GATEWAY_QUEUE = 'RABBITMQ_GATEWAY_QUEUE',
  RABBITMQ_MARKET_QUEUE = 'RABBITMQ_MARKET_QUEUE',

  MONGO_URI = 'MONGO_URI',
}
