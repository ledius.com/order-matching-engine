import { Module } from '@nestjs/common';
import { EnvModule } from '@ledius/env';

@Module({
  imports: [EnvModule],
  exports: [EnvModule],
})
export class LocalEnvModule {}
