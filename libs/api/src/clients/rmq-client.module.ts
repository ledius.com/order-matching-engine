import { DynamicModule, Module } from '@nestjs/common';
import { ClientsModule, RmqOptions, Transport } from '@nestjs/microservices';
import { services } from '@libs/api/clients/constants';
import { EnvProviderService } from '@ledius/env';
import { LocalEnvEnum, LocalEnvModule } from '@libs/local-env';

@Module({})
export class RmqClientModule {
  public static async forRoot(): Promise<DynamicModule> {
    const providers = [
      ClientsModule.registerAsync([
        {
          imports: [LocalEnvModule],
          name: services.ExchangeService,
          useFactory: (env: EnvProviderService) =>
            ({
              transport: Transport.RMQ,
              options: {
                urls: [env.getOrFail(LocalEnvEnum.RABBITMQ_URI)],
                queue: env.getOrFail(LocalEnvEnum.RABBITMQ_EXCHANGE_QUEUE),
                queueOptions: {
                  durable: false,
                },
              },
            } as RmqOptions),
          inject: [EnvProviderService],
        },
        {
          imports: [LocalEnvModule],
          name: services.GatewayService,
          useFactory: (env: EnvProviderService) =>
            ({
              transport: Transport.RMQ,
              options: {
                urls: [env.getOrFail(LocalEnvEnum.RABBITMQ_URI)],
                queue: env.getOrFail(LocalEnvEnum.RABBITMQ_GATEWAY_QUEUE),
                queueOptions: {
                  durable: false,
                },
              },
            } as RmqOptions),
          inject: [EnvProviderService],
        },
        {
          imports: [LocalEnvModule],
          name: services.MarketService,
          useFactory: (env: EnvProviderService) =>
            ({
              transport: Transport.RMQ,
              options: {
                urls: [env.getOrFail(LocalEnvEnum.RABBITMQ_URI)],
                queue: env.getOrFail(LocalEnvEnum.RABBITMQ_MARKET_QUEUE),
                queueOptions: {
                  durable: false,
                },
              },
            } as RmqOptions),
          inject: [EnvProviderService],
        },
      ]),
    ];

    return {
      global: true,
      module: RmqClientModule,
      imports: providers,
      exports: providers,
    };
  }
}
