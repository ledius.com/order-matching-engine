import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsString } from 'class-validator';

export class TradingPairDto {
  @IsDefined()
  @IsString()
  @ApiProperty({
    default: 'btc',
  })
  public base: string;

  @IsDefined()
  @IsString()
  @ApiProperty({
    default: 'usd',
  })
  public quote: string;
}
