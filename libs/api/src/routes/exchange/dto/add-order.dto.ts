import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDefined, IsEnum, IsNumberString } from 'class-validator';
import { DirectionEnum } from '@libs/api/common/direction.enum';
import { TypeEnum } from '@libs/api/common/type.enum';
import { TradingPairDto } from '@libs/api/routes/exchange/dto/trading-pair.dto';

export class AddOrderDto {
  @IsDefined()
  @ApiProperty({
    type: () => TradingPairDto,
  })
  @Type(() => TradingPairDto)
  public tradingPair: TradingPairDto;

  @IsDefined()
  @IsNumberString()
  @ApiProperty({
    default: '1000',
  })
  public price: string;

  @IsDefined()
  @IsNumberString()
  @ApiProperty({
    default: '10',
  })
  public quantity: string;

  @IsDefined()
  @IsEnum(DirectionEnum)
  @ApiProperty({
    type: 'number',
    enum: DirectionEnum,
    default: DirectionEnum.BUY,
  })
  @Type(() => Number)
  public direction: DirectionEnum;

  @IsDefined()
  @IsEnum(TypeEnum)
  @ApiProperty({
    type: 'number',
    enum: TypeEnum,
    default: TypeEnum.LIMIT,
  })
  @Type(() => Number)
  public type: TypeEnum = TypeEnum.LIMIT;
}
