export const exchange = {
  addOrder: 'exchange_add_order',

  events: {
    onNewOrder: 'exchange_events_on_new_order',
    onTrade: 'exchange_events_on_trade',
  },
};
