import { TradingPairDto } from '@libs/api/routes/exchange/dto/trading-pair.dto';
import { DirectionEnum } from '@libs/api/common/direction.enum';
import { TypeEnum } from '@libs/api/common/type.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class OrderResponse {
  @ApiProperty({
    type: TradingPairDto,
  })
  @Type(() => TradingPairDto)
  readonly pair: TradingPairDto;

  @ApiProperty()
  readonly id: string;

  @ApiProperty({
    type: 'string',
  })
  @Type(() => BigInt)
  readonly price: bigint;

  @ApiProperty({
    enum: DirectionEnum,
  })
  @Type(() => Number)
  readonly direction: DirectionEnum;

  @ApiProperty({
    enum: TypeEnum,
  })
  @Type(() => Number)
  readonly type: TypeEnum;

  @ApiProperty({
    type: 'string',
  })
  @Type(() => BigInt)
  readonly quantity: bigint;
}
