import { tradingPair } from '@libs/api/routes/trading-pair/trading-pair';
import { exchange } from '@libs/api/routes/exchange/exchange';
import { market } from '@libs/api/routes/market/market';

export const routes = {
  tradingPair,
  exchange,
  market,
};
