export enum OrderExecutionStatusEnum {
  PARTIAL = 1,
  FULL = 2,
}
