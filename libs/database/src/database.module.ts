import { EnvProviderService } from '@ledius/env';
import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleFactoryOptions } from '@nestjs/mongoose';
import { LocalEnvEnum, LocalEnvModule } from '@libs/local-env';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [LocalEnvModule],
      inject: [EnvProviderService],
      useFactory: (env: EnvProviderService) =>
        ({
          uri: env.getOrFail(LocalEnvEnum.MONGO_URI),
        } as MongooseModuleFactoryOptions),
    }),
  ],
})
export class DatabaseModule {}
