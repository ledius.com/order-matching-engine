up:
	docker-compose up -d
down:
	docker-compose down --remove-orphans
log:
	docker-compose logs -f order-matching-engine order-matching-engine-gateway order-matching-engine-market
