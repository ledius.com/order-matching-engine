import {
  ConsoleLogger,
  Inject,
  Injectable,
  OnModuleInit,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Exchange } from '@exchange/exchange/exchange';
import { routes, services } from '@libs/api';
import { OrderResponse } from '@libs/api/routes/exchange/response/order.response';
import { TradeData } from '@exchange/exchange/trade-data';

@Injectable()
export class EventService implements OnModuleInit {
  private readonly logger = new ConsoleLogger(EventService.name);

  constructor(
    @Inject(services.GatewayService) private readonly client: ClientProxy,
    private readonly exchange: Exchange,
  ) {}

  public async onModuleInit(): Promise<void> {
    this.exchange.onNewOrder$.subscribe((order) => {
      this.client.emit(routes.exchange.events.onNewOrder, {
        direction: order.direction,
        quantity: order.quantity,
        id: order.id,
        price: order.price,
        pair: order.pair,
        type: order.type,
      } as OrderResponse);
    });
    this.logger.log(`apply event: ${routes.exchange.events.onNewOrder}`);

    this.exchange.onTrade$.subscribe(async (trade) => {
      this.client.emit(
        routes.exchange.events.onTrade,
        await new TradeData(trade).resolve(),
      );
    });

    this.logger.log(`apply event: ${routes.exchange.events.onTrade}`);
  }
}
