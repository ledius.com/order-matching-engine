import { TradeInterface } from './interfaces/trade.interface';

import { DirectionEnum } from '../book/enums/direction.enum';
import { TypeEnum } from '../book/enums/type.enum';
import { TradingPairInterface } from '../trading-pair/interfaces/trading-pair.interface';

export interface TradeDataInterface {
  ask: {
    price: bigint;
    quantity: bigint;
    direction: DirectionEnum;
    type: TypeEnum;
    id: string;
  };
  bid: {
    price: bigint;
    quantity: bigint;
    direction: DirectionEnum;
    type: TypeEnum;
    id: string;
  };
  pair: TradingPairInterface;
  createdAt: Date;
}

export class TradeData {
  constructor(private readonly trade: TradeInterface) {}

  public async resolve(): Promise<TradeDataInterface> {
    const [ask, bid, pair] = await Promise.all([
      this.trade.getAskOrder(),
      this.trade.getBidOrder(),
      this.trade.getPair(),
    ]);

    return {
      ask: {
        price: ask.price,
        quantity: ask.quantity,
        id: ask.id,
        direction: ask.direction,
        type: ask.type,
      },
      bid: {
        price: bid.price,
        quantity: bid.quantity,
        id: bid.id,
        direction: bid.direction,
        type: bid.type,
      },
      pair: pair,
      createdAt: this.trade.getCreatedAt(),
    };
  }
}
