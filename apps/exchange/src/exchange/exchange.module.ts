import { Module } from '@nestjs/common';
import { Connection, Model } from 'mongoose';
import { getConnectionToken } from '@nestjs/mongoose';
import { getModelToken } from '@nestjs/mongoose';
import { Exchange } from './exchange';
import {
  TradingPair,
  TradingPairDocument,
} from '../trading-pair/dao/trading-pair';
import { mongoOrderSchema } from '../book/mongo-book/mongo-order-model';
import { TradingPairModule } from '../trading-pair/trading-pair.module';
import { MongoOrderBook } from '../book/mongo-book/mongo-order-book';
import { ExchangeController } from './controller/exchange.controller';
import { routes, services } from '@libs/api';
import { ClientProxy } from '@nestjs/microservices';
import { OrderResponse } from '@libs/api/routes/exchange/response/order.response';
import { TradeData } from './trade-data';
import { EventService } from '@exchange/exchange/services/event.service';

@Module({
  imports: [TradingPairModule],
  controllers: [ExchangeController],
  providers: [
    EventService,
    {
      provide: Exchange,
      inject: [
        getConnectionToken(),
        getModelToken(TradingPair.name),
        services.GatewayService,
      ],
      useFactory: async (
        connection: Connection,
        tradingPairModel: Model<TradingPairDocument>,
      ) => {
        const exchange = new Exchange();

        const pairs = await tradingPairModel.find({}).exec();

        for (const pair of pairs) {
          const model = connection.model(
            `${pair.base}_${pair.quote}_order_book`,
            mongoOrderSchema,
            `${pair.base}_${pair.quote}_order_book`,
          );
          await exchange.addBook(pair, new MongoOrderBook(model));
        }

        return exchange;
      },
    },
  ],
  exports: [Exchange],
})
export class ExchangeModule {}
