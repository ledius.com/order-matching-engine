import { Controller } from '@nestjs/common';
import { Exchange } from '../exchange';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { AddOrderDto } from '@libs/api/routes/exchange/dto/add-order.dto';
import { routes } from '@libs/api';
import { OrderResponse } from '@libs/api/routes/exchange/response/order.response';
import { plainToInstance } from 'class-transformer';

@Controller()
export class ExchangeController {
  constructor(private readonly exchange: Exchange) {}

  @MessagePattern(routes.exchange.addOrder)
  public async addOrder(@Payload() dto: AddOrderDto): Promise<OrderResponse> {
    const order = await this.exchange.addOrder({
      pair: dto.tradingPair,
      type: Number(dto.type),
      quantity: BigInt(dto.quantity),
      direction: Number(dto.direction),
      price: BigInt(dto.price),
    });

    return {
      type: order.type,
      direction: order.direction,
      pair: order.pair,
      price: order.price,
      id: order.id,
      quantity: order.quantity,
    };
  }
}
