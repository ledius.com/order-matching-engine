import {
  ExchangeInterface,
  ExchangeOrderInterface,
} from './interfaces/exchange.interface';
import {
  defer,
  from,
  lastValueFrom,
  map,
  Observable,
  Subject,
  switchMap,
} from 'rxjs';
import { TradeInterface } from './interfaces/trade.interface';
import { ConsoleLogger, Injectable } from '@nestjs/common';
import { Trade } from './trade';
import { OrderBookInterface } from '../book/interfaces/order-book.interface';
import { TradingPairInterface } from '../trading-pair/interfaces/trading-pair.interface';
import { VolumeInterface } from './interfaces/volume.interface';

@Injectable()
export class Exchange implements ExchangeInterface {
  private readonly logger: ConsoleLogger = new ConsoleLogger('Exchange');
  private readonly orderBooks: Map<string, OrderBookInterface> = new Map();
  private readonly tradingPairs: Set<TradingPairInterface> =
    new Set<TradingPairInterface>([]);

  onCancel$: Subject<ExchangeOrderInterface> =
    new Subject<ExchangeOrderInterface>();
  onTrade$: Subject<TradeInterface> = new Subject<TradeInterface>();
  onNewOrder$: Subject<ExchangeOrderInterface> =
    new Subject<ExchangeOrderInterface>();

  public async addBook(
    tradingPair: TradingPairInterface,
    orderBook: OrderBookInterface,
  ): Promise<void> {
    const key = this.getBookKey(tradingPair);
    const exists = this.orderBooks.has(key);
    if (!exists) {
      this.orderBooks.set(key, orderBook);
      this.tradingPairs.add(tradingPair);
      this.logger.log(`Add ${tradingPair.base}/${tradingPair.quote} book`);
    }
  }

  public async addOrder(
    exchangeOrder: Omit<ExchangeOrderInterface, 'id'>,
  ): Promise<ExchangeOrderInterface> {
    const book = await this.getBook(exchangeOrder.pair);

    const order = await book.addOrder(
      exchangeOrder.price,
      exchangeOrder.quantity,
      exchangeOrder.direction,
      exchangeOrder.type,
    );

    this.onNewOrder$.next({
      ...exchangeOrder,
      id: order.id,
    });

    await this.trade(exchangeOrder.pair, exchangeOrder.price);

    return { ...order, pair: exchangeOrder.pair };
  }

  public async trade(
    tradingPair: TradingPairInterface,
    price?: bigint,
  ): Promise<TradeInterface | undefined> {
    const book = await this.getBook(tradingPair);
    const [bid, ask] = await Promise.all([
      book.getMaxBid(price),
      book.getMinAsk(price),
    ]);

    if (!bid || !ask) {
      return;
    }

    if (bid.price === ask.price) {
      const trade = new Trade(
        { ...ask, pair: tradingPair },
        { ...bid, pair: tradingPair },
      );
      await Promise.all([
        book.executeOrder(
          bid.id,
          await trade.getBidExecutionQuantity(),
          await trade.getBidOrderExecutionStatus(),
        ),
        book.executeOrder(
          ask.id,
          await trade.getAskExecutionQuantity(),
          await trade.getAskOrderExecutionStatus(),
        ),
      ]);
      this.onTrade$.next(trade);

      return trade;
    }

    return undefined;
  }

  public async getBook(
    tradingPair: TradingPairInterface,
  ): Promise<OrderBookInterface> {
    const key = this.getBookKey(tradingPair);
    const book = this.orderBooks.get(key);
    if (!book) {
      throw new Error(`Book ${key} not found`);
    }
    return book;
  }

  public getBookKey(tradingPair: TradingPairInterface): string {
    return `${tradingPair.base}/${tradingPair.quote}`;
  }

  public async getTradingPairs(): Promise<TradingPairInterface[]> {
    return Array.from(this.tradingPairs.values());
  }

  getVolume(
    tradingPair: TradingPairInterface,
    depth?: number,
  ): Observable<VolumeInterface> {
    return defer(() => from(this.getBook(tradingPair))).pipe(
      switchMap((book) => book.getVolume(depth)),
      map((volume) => ({
        asks: volume.asks,
        bids: volume.bids,
        tradingPair,
      })),
    );
  }
}
