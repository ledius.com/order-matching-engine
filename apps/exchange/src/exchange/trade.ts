import { TradeInterface } from './interfaces/trade.interface';
import { ExchangeOrderInterface } from './interfaces/exchange.interface';
import { OrderExecutionStatusEnum } from '../book/enums/order-execution-status.enum';
import { TradingPairInterface } from '../trading-pair/interfaces/trading-pair.interface';

export class Trade implements TradeInterface {
  constructor(
    private readonly askOrder: ExchangeOrderInterface,
    private readonly bidOrder: ExchangeOrderInterface,
    private readonly createdAt: Date = new Date(),
  ) {}

  public async getAskOrder(): Promise<ExchangeOrderInterface> {
    return this.askOrder;
  }

  public async getBidOrder(): Promise<ExchangeOrderInterface> {
    return this.bidOrder;
  }

  public async getPair(): Promise<TradingPairInterface> {
    return this.askOrder.pair;
  }

  public async getAskExecutionQuantity(): Promise<bigint> {
    if (this.bidOrder.quantity > this.askOrder.quantity) {
      return this.askOrder.quantity;
    }

    return this.bidOrder.quantity;
  }

  public async getAskOrderExecutionStatus(): Promise<OrderExecutionStatusEnum> {
    const quantity = await this.getAskExecutionQuantity();
    if (quantity === this.askOrder.quantity) {
      return OrderExecutionStatusEnum.FULL;
    }

    return OrderExecutionStatusEnum.PARTIAL;
  }

  public async getBidExecutionQuantity(): Promise<bigint> {
    if (this.askOrder.quantity > this.bidOrder.quantity) {
      return this.bidOrder.quantity;
    }

    return this.askOrder.quantity;
  }

  public async getBidOrderExecutionStatus(): Promise<OrderExecutionStatusEnum> {
    const quantity = await this.getBidExecutionQuantity();
    if (quantity === this.bidOrder.quantity) {
      return OrderExecutionStatusEnum.FULL;
    }

    return OrderExecutionStatusEnum.PARTIAL;
  }

  public getCreatedAt(): Date {
    return this.createdAt;
  }
}
