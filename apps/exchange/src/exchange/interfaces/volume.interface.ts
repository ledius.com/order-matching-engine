import { OrderInterface } from '../../book/interfaces/order.interface';
import { TradingPairInterface } from '../../trading-pair/interfaces/trading-pair.interface';

export interface VolumeInterface {
  asks: OrderInterface[];
  bids: OrderInterface[];
  tradingPair: TradingPairInterface;
}
