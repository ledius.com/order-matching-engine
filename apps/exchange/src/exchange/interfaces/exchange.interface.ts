import { Observable, Subject } from 'rxjs';
import { TradeInterface } from './trade.interface';
import { OrderInterface } from '../../book/interfaces/order.interface';
import { OrderBookInterface } from '../../book/interfaces/order-book.interface';
import { TradingPairInterface } from '../../trading-pair/interfaces/trading-pair.interface';
import { VolumeInterface } from './volume.interface';

export interface ExchangeOrderInterface
  extends Omit<OrderInterface, 'subQuantity'> {
  readonly pair: TradingPairInterface;
}

export interface ExchangeInterface {
  getTradingPairs(): Promise<TradingPairInterface[]>;
  addBook(
    pair: TradingPairInterface,
    orderBook: OrderBookInterface,
  ): Promise<void>;
  addOrder(
    exchangeOrder: Omit<ExchangeOrderInterface, 'id'>,
  ): Promise<ExchangeOrderInterface>;
  getVolume(
    tradingPair: TradingPairInterface,
    depth?: number,
  ): Observable<VolumeInterface>;

  onTrade$: Observable<TradeInterface>;
  onCancel$: Observable<ExchangeOrderInterface>;
  onNewOrder$: Subject<ExchangeOrderInterface>;
}
