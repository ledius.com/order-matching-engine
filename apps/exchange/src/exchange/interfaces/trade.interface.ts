import { ExchangeOrderInterface } from './exchange.interface';
import { OrderExecutionStatusEnum } from '../../book/enums/order-execution-status.enum';
import { TradingPairInterface } from '../../trading-pair/interfaces/trading-pair.interface';

export interface TradeInterface {
  getPair(): Promise<TradingPairInterface>;
  getAskOrder(): Promise<ExchangeOrderInterface>;
  getBidOrder(): Promise<ExchangeOrderInterface>;
  getAskOrderExecutionStatus(): Promise<OrderExecutionStatusEnum>;
  getBidOrderExecutionStatus(): Promise<OrderExecutionStatusEnum>;
  getAskExecutionQuantity(): Promise<bigint>;
  getBidExecutionQuantity(): Promise<bigint>;
  getCreatedAt(): Date;
}
