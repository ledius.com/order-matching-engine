import { Module } from '@nestjs/common';
import { TradingPairModule } from './trading-pair/trading-pair.module';
import { ExchangeModule } from './exchange/exchange.module';
import { RmqClientModule } from '@libs/api';
import { DatabaseModule } from '@libs/database/database.module';

@Module({
  imports: [
    DatabaseModule,
    TradingPairModule,
    ExchangeModule,
    RmqClientModule.forRoot(),
  ],
})
export class AppModule {}
