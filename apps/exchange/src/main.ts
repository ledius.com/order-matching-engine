import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { EnvProviderService } from '@ledius/env';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { defer, delayWhen, from, map, mergeMap, tap } from 'rxjs';
import { ValidationPipe } from '@nestjs/common';
import '@libs/big-int-json';
import { LocalEnvEnum, LocalEnvModule } from '@libs/local-env';

defer(() => from(NestFactory.createApplicationContext(LocalEnvModule)))
  .pipe(
    map((app) => app.get(EnvProviderService)),
    mergeMap((env) =>
      NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
        transport: Transport.RMQ,
        options: {
          urls: [env.getOrFail(LocalEnvEnum.RABBITMQ_URI)],
          queue: env.getOrFail(LocalEnvEnum.RABBITMQ_EXCHANGE_QUEUE),
          queueOptions: {
            durable: false,
          },
        },
      }),
    ),
    tap((app) =>
      app.useGlobalPipes(
        new ValidationPipe({
          transform: true,
          enableDebugMessages: true,
        }),
      ),
    ),
    delayWhen((app) => app.listen()),
  )
  .subscribe(() => {});
