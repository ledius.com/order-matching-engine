import { DirectionEnum } from '../enums/direction.enum';
import { TypeEnum } from '../enums/type.enum';
import { HydratedDocument } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type MongoOrderModelDocument = HydratedDocument<MongoOrderModel>;

@Schema()
export class MongoOrderModel {
  @Prop({
    required: true,
    type: String,
    index: true,
  })
  readonly direction: DirectionEnum;

  @Prop({
    required: true,
    type: String,
    index: true,
  })
  readonly price: string;

  @Prop({
    required: true,
    type: String,
  })
  quantity: string;

  @Prop({
    required: true,
    type: String,
  })
  readonly type: TypeEnum;
}

export const mongoOrderSchema = SchemaFactory.createForClass(MongoOrderModel);
