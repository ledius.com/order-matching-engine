import { OrderInterface } from '../interfaces/order.interface';
import { OrderBookInterface } from '../interfaces/order-book.interface';
import { OrderExecutionStatusEnum } from '../enums/order-execution-status.enum';
import { TypeEnum } from '../enums/type.enum';
import { DirectionEnum } from '../enums/direction.enum';
import { MongoOrder } from './mongo-order';
import { Model, Types } from 'mongoose';
import { MongoOrderModel } from './mongo-order-model';
import { InjectModel } from '@nestjs/mongoose';

export class MongoOrderBook implements OrderBookInterface {
  constructor(
    @InjectModel(MongoOrderModel.name)
    private readonly mongoOrderModel: Model<MongoOrderModel>,
  ) {}

  public async addOrder(
    price: bigint,
    quantity: bigint,
    direction: DirectionEnum,
    type: TypeEnum,
  ): Promise<OrderInterface> {
    return this.mongoOrderModel
      .create({
        price: String(price),
        type: type,
        direction,
        quantity: String(quantity),
      })
      .then((_) => _.save())
      .then((document) => new MongoOrder(document));
  }

  public async cancelOrder(orderId: string): Promise<void> {
    await this.mongoOrderModel.findByIdAndDelete(orderId);
  }

  public async executeOrder(
    orderId: string,
    quantity: bigint,
    status: OrderExecutionStatusEnum,
  ): Promise<void> {
    if (status === OrderExecutionStatusEnum.FULL) {
      await this.mongoOrderModel
        .deleteOne({
          _id: new Types.ObjectId(orderId),
        })
        .exec();
      return;
    }

    await this.mongoOrderModel
      .findOne({ _id: new Types.ObjectId(orderId) })
      .exec()
      .then((doc) => new MongoOrder(doc))
      .then((order) => order.subQuantity(quantity))
      .then(({ document }) =>
        this.mongoOrderModel.updateOne(
          {
            _id: document._id,
          },
          document,
        ),
      );
  }

  public async getAsks(depth = 10): Promise<OrderInterface[]> {
    const documents = await this.mongoOrderModel
      .find(
        {
          quantity: DirectionEnum.SELL,
        },
        null,
        {
          limit: depth,
        },
      )
      .exec();

    return documents.map((document) => new MongoOrder(document));
  }

  public async getBids(depth?: number): Promise<OrderInterface[]> {
    const documents = await this.mongoOrderModel
      .find(
        {
          quantity: DirectionEnum.BUY,
        },
        null,
        {
          limit: depth,
        },
      )
      .exec();

    return documents.map((document) => new MongoOrder(document));
  }

  public async getMaxBid(price?: bigint): Promise<OrderInterface | undefined> {
    const order = await this.mongoOrderModel
      .findOne({
        direction: DirectionEnum.BUY,
        price: price ? String(price) : price,
      })
      .limit(1)
      .sort({
        price: -1,
      })
      .exec();

    if (!order) {
      return undefined;
    }

    return new MongoOrder(order);
  }

  public async getMinAsk(price?: bigint): Promise<OrderInterface | undefined> {
    const order = await this.mongoOrderModel
      .findOne({
        direction: DirectionEnum.SELL,
        price: price ? String(price) : price,
      })
      .limit(1)
      .sort({
        price: 1,
      })
      .exec();

    if (!order) {
      return undefined;
    }

    return new MongoOrder(order);
  }

  public async getVolume(
    depth = 10,
  ): Promise<{ asks: OrderInterface[]; bids: OrderInterface[] }> {
    return { asks: await this.getAsks(depth), bids: await this.getBids(depth) };
  }
}
