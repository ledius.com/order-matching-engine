import { OrderInterface } from '../interfaces/order.interface';
import { MongoOrderModelDocument } from './mongo-order-model';
import { DirectionEnum } from '../enums/direction.enum';
import { TypeEnum } from '../enums/type.enum';

export class MongoOrder implements OrderInterface {
  readonly id: string;

  readonly direction: DirectionEnum;
  readonly price: bigint;
  quantity: bigint;
  readonly type: TypeEnum;

  constructor(public readonly document: MongoOrderModelDocument) {
    this.id = document._id.toHexString();
    this.direction = document.direction;
    this.price = BigInt(document.price);
    this.quantity = BigInt(document.quantity);
    this.type = document.type;
  }

  async subQuantity(quantity: bigint): Promise<MongoOrder> {
    this.quantity = this.quantity - quantity;
    this.document.quantity = String(this.quantity);

    return this;
  }
}
