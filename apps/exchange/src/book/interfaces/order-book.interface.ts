import { OrderInterface } from './order.interface';
import { DirectionEnum } from '../enums/direction.enum';
import { TypeEnum } from '../enums/type.enum';
import { ReplaySubject } from 'rxjs';
import { OrderExecutionStatusEnum } from '../enums/order-execution-status.enum';

export interface OrderBookInterface {
  getVolume(
    depth?: number,
  ): Promise<{ asks: OrderInterface[]; bids: OrderInterface[] }>;
  getAsks(depth?: number): Promise<OrderInterface[]>;
  getBids(depth?: number): Promise<OrderInterface[]>;

  addOrder(
    price: bigint,
    quantity: bigint,
    direction: DirectionEnum,
    type: TypeEnum,
  ): Promise<OrderInterface>;

  cancelOrder(orderId: string): Promise<void>;
  executeOrder(
    orderId: string,
    quantity: bigint,
    status: OrderExecutionStatusEnum,
  ): Promise<void>;

  getMaxBid(price?: bigint): Promise<OrderInterface | undefined>;
  getMinAsk(price?: bigint): Promise<OrderInterface | undefined>;
}
