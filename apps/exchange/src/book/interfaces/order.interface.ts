import { DirectionEnum } from '../enums/direction.enum';
import { TypeEnum } from '../enums/type.enum';

export interface OrderInterface {
  readonly id: string;
  readonly price: bigint;
  readonly direction: DirectionEnum;
  readonly type: TypeEnum;
  readonly quantity: bigint;

  subQuantity(quantity: bigint): Promise<OrderInterface>;
}
