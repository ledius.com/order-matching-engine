import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsString } from 'class-validator';

export class CreatePairDto {
  @ApiProperty()
  @IsString()
  @IsDefined()
  base: string;

  @ApiProperty()
  @IsString()
  @IsDefined()
  quote: string;
}
