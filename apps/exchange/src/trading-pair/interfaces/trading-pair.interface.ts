export interface TradingPairInterface {
  base: string;
  quote: string;
}
