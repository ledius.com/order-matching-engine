import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TradingPair, tradingPairSchema } from './dao/trading-pair';
import { TradingPairController } from './controller/trading-pair.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: TradingPair.name, schema: tradingPairSchema },
    ]),
  ],
  controllers: [TradingPairController],
  exports: [
    MongooseModule.forFeature([
      { name: TradingPair.name, schema: tradingPairSchema },
    ]),
  ],
})
export class TradingPairModule {}
