import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { TradingPairInterface } from '../interfaces/trading-pair.interface';

export type TradingPairDocument = HydratedDocument<TradingPair>;

@Schema({
  collection: 'trading_pairs',
})
export class TradingPair implements TradingPairInterface {
  @Prop({
    type: String,
  })
  public base: string;

  @Prop({
    type: String,
  })
  public quote: string;
}

export const tradingPairSchema = SchemaFactory.createForClass(TradingPair);
