import { ConflictException, Controller } from '@nestjs/common';
import { Model } from 'mongoose';
import { TradingPair, TradingPairDocument } from '../dao/trading-pair';
import { InjectModel } from '@nestjs/mongoose';
import { CreatePairDto } from '../dto/create-pair.dto';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { routes } from '@libs/api';

@Controller()
export class TradingPairController {
  constructor(
    @InjectModel(TradingPair.name)
    private readonly tradingPairModel: Model<TradingPairDocument>,
  ) {}

  @MessagePattern(routes.tradingPair.list)
  public async list(): Promise<TradingPairDocument[]> {
    return this.tradingPairModel.find().exec();
  }

  @MessagePattern(routes.tradingPair.create)
  public async create(
    @Payload() dto: CreatePairDto,
  ): Promise<TradingPairDocument> {
    const alreadyExists = await this.tradingPairModel.findOne(dto).exec();

    if (alreadyExists) {
      throw new ConflictException(
        `Pair ${dto.base}/${dto.quote} already exists`,
      );
    }

    return new this.tradingPairModel(dto).save();
  }
}
