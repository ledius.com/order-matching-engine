import { Module } from '@nestjs/common';
import { RmqClientModule } from '@libs/api';
import { PriceDataModule } from '@market/price-data/price-data.module';
import { DatabaseModule } from '@libs/database/database.module';

@Module({
  imports: [PriceDataModule, RmqClientModule.forRoot(), DatabaseModule],
})
export class AppModule {}
