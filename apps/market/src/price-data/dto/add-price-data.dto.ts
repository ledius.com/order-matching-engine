import { IsDate, IsDefined, IsNumberString, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class AddPriceDataDto {
  @IsDefined()
  @IsString()
  @ApiProperty({
    type: 'string',
    example: 'btcusd',
  })
  public symbol: string;

  @IsDefined()
  @IsNumberString()
  @ApiProperty({
    type: 'string',
    example: '10000',
  })
  public price: bigint;

  @IsDefined()
  @IsDate()
  @Type(() => Date)
  @ApiProperty({
    type: 'string',
    example: new Date().toISOString(),
  })
  public time: Date;
}
