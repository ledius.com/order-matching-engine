import { IsDefined, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AggregatePriceDataDto {
  @IsDefined()
  @IsString()
  @ApiProperty({
    example: 'btcusd',
  })
  public symbol: string;
}
