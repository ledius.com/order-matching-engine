import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PriceData, priceDataSchema } from '@market/price-data/dao/price-data';
import { PriceDataController } from '@market/price-data/controller/price-data.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: PriceData.name, schema: priceDataSchema },
    ]),
  ],
  controllers: [PriceDataController],
})
export class PriceDataModule {}
