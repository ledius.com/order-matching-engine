import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type PriceDataDocument = HydratedDocument<PriceData>;

@Schema({
  collection: 'price_data',
  timeseries: {
    timeField: 'time',
    granularity: 'seconds',
    metaField: 'symbol',
  },
})
export class PriceData {
  @Prop()
  public symbol: string;

  @Prop({
    type: String,
  })
  public price: string;

  @Prop({
    type: Date,
  })
  public time: Date;
}

export const priceDataSchema = SchemaFactory.createForClass(PriceData);
