import { Controller } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  PriceData,
  PriceDataDocument,
} from '@market/price-data/dao/price-data';
import { InjectModel } from '@nestjs/mongoose';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { routes } from '@libs/api';
import { AddPriceDataDto } from '@market/price-data/dto/add-price-data.dto';
import { AggregatePriceDataDto } from '@market/price-data/dto/aggregate-price-data.dto';
import { PriceDataAggregate } from '@market/price-data/controller/price-data-aggregate';

@Controller()
export class PriceDataController {
  constructor(
    @InjectModel(PriceData.name)
    private readonly priceDataModel: Model<PriceData>,
  ) {}

  @MessagePattern(routes.market.priceData.add)
  public async add(
    @Payload() data: AddPriceDataDto,
  ): Promise<PriceDataDocument> {
    const model = new this.priceDataModel({
      price: data.price.toString(),
      symbol: data.symbol,
      time: data.time,
    });

    return await model.save();
  }

  @MessagePattern(routes.market.priceData.aggregate)
  public async aggregate(
    @Payload() data: AggregatePriceDataDto,
  ): Promise<PriceDataAggregate[]> {
    return this.priceDataModel.aggregate<PriceDataAggregate>([
      {
        $match: {
          symbol: data.symbol,
        },
      },
      {
        $sort: {
          time: -1,
        },
      },
      {
        $addFields: {
          numberPrice: { $toInt: '$price' },
        },
      },
      {
        $group: {
          _id: {
            $dateTrunc: {
              date: '$time',
              unit: 'second',
              binSize: 5,
            },
          } as never,
          symbol: { $first: '$symbol' },
          price: { $avg: '$numberPrice' },
          open: { $last: '$numberPrice' },
          close: { $first: '$numberPrice' },
          min: { $min: '$numberPrice' },
          max: { $max: '$numberPrice' },
        },
      },
      {
        $addFields: {
          time: '$_id',
        },
      },
      {
        $unset: ['_id'],
      },
    ]);
  }
}
