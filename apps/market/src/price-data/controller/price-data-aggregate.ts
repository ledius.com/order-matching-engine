export interface PriceDataAggregate {
  time: Date;
  symbol: string;
  price: string;
  open: string;
  close: string;
  min: string;
  max: string;
}
