import { defer, delayWhen, from, map, switchMap, tap } from 'rxjs';
import { NestFactory } from '@nestjs/core';
import { LocalEnvEnum, LocalEnvModule } from '@libs/local-env';
import { EnvProviderService } from '@ledius/env';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from '@market/app.module';
import { ValidationPipe } from '@nestjs/common';

defer(() => from(NestFactory.createApplicationContext(LocalEnvModule)))
  .pipe(
    map((app) => app.get(EnvProviderService)),
    switchMap((env) =>
      NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
        transport: Transport.RMQ,
        options: {
          urls: [env.getOrFail(LocalEnvEnum.RABBITMQ_URI)],
          queue: env.getOrFail(LocalEnvEnum.RABBITMQ_MARKET_QUEUE),
          queueOptions: {
            durable: false,
          },
        },
      }),
    ),
    tap((app) =>
      app.useGlobalPipes(
        new ValidationPipe({
          transform: true,
          enableDebugMessages: true,
        }),
      ),
    ),
    delayWhen((app) => app.listen()),
  )
  .subscribe(() => {});
