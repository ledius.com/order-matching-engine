import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { concat, defer, delayWhen, from, map, tap } from 'rxjs';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { EnvProviderService } from '@ledius/env';
import { ValidationPipe } from '@nestjs/common';
import { LocalEnvEnum } from '@libs/local-env';

defer(() => from(NestFactory.create(AppModule)))
  .pipe(
    map((app) => ({ app, env: app.get(EnvProviderService) })),
    tap(({ app }) => {
      const config = new DocumentBuilder().setTitle('Exchange').build();
      const document = SwaggerModule.createDocument(app, config);
      SwaggerModule.setup('/api/docs', app, document);
    }),
    tap(({ app, env }) =>
      app.connectMicroservice<MicroserviceOptions>({
        transport: Transport.RMQ,
        options: {
          urls: [env.getOrFail(LocalEnvEnum.RABBITMQ_URI)],
          queue: env.getOrFail(LocalEnvEnum.RABBITMQ_GATEWAY_QUEUE),
          queueOptions: {
            durable: false,
          },
        },
      }),
    ),
    tap(({ app }) =>
      app.useGlobalPipes(
        new ValidationPipe({
          transform: true,
          enableDebugMessages: true,
        }),
      ),
    ),
    delayWhen(({ app }) =>
      concat(app.listen(3000), app.startAllMicroservices()),
    ),
  )
  .subscribe(() => {});
