import { Module } from '@nestjs/common';
import { TradingPairController } from './trading-pair.controller';

@Module({
  controllers: [TradingPairController],
})
export class TradingPairModule {}
