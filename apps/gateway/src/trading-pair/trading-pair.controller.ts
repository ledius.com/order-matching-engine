import { Body, Controller, Get, Inject, Post } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { routes, services } from '@libs/api';
import { TradingPairDto } from '@libs/api/routes/exchange/dto/trading-pair.dto';
import { lastValueFrom } from 'rxjs';
import { ApiTags } from '@nestjs/swagger';

@Controller()
@ApiTags('Trading Pairs')
export class TradingPairController {
  constructor(
    @Inject(services.ExchangeService)
    private readonly exchangeService: ClientProxy,
  ) {}

  @Post('/trading-pair')
  public async create(@Body() dto: TradingPairDto): Promise<TradingPairDto> {
    return lastValueFrom(
      this.exchangeService.send(routes.tradingPair.create, dto),
    );
  }

  @Get('/trading-pair')
  public async list(): Promise<TradingPairDto[]> {
    return lastValueFrom(
      this.exchangeService.send(routes.tradingPair.list, {}),
    );
  }
}
