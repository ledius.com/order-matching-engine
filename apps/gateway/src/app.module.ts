import { Module } from '@nestjs/common';
import { RmqClientModule } from '@libs/api';
import { TradingPairModule } from './trading-pair/trading-pair.module';
import { ExchangeModule } from './exchange/exchange.module';
import { MarketModule } from '@gateway/market/market.module';

@Module({
  imports: [
    RmqClientModule.forRoot(),
    TradingPairModule,
    ExchangeModule,
    MarketModule,
  ],
})
export class AppModule {}
