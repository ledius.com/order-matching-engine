import {
  SubscribeMessage,
  WebSocketGateway,
  WsResponse,
} from '@nestjs/websockets';
import { map, Observable, Subject } from 'rxjs';
import { routes } from '@libs/api';
import { OrderResponse } from '@libs/api/routes/exchange/response/order.response';
import { TradeDataInterface } from '@exchange/exchange/trade-data';

@WebSocketGateway({ cors: true })
export class ExchangeGateway {
  readonly onNewOrder$: Subject<OrderResponse> = new Subject<OrderResponse>();
  readonly onTrade$: Subject<TradeDataInterface> =
    new Subject<TradeDataInterface>();

  @SubscribeMessage(routes.exchange.events.onNewOrder)
  public onAddOrder(): Observable<WsResponse<string>> {
    return this.onNewOrder$.pipe(
      map((order) => ({
        event: routes.exchange.events.onNewOrder,
        data: JSON.stringify(order),
      })),
    );
  }

  @SubscribeMessage(routes.exchange.events.onTrade)
  public onTrade(): Observable<WsResponse<string>> {
    return this.onTrade$.pipe(
      map((trade) => ({
        event: routes.exchange.events.onTrade,
        data: JSON.stringify(trade),
      })),
    );
  }
}
