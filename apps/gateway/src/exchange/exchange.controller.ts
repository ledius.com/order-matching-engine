import { Body, Controller, Inject, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ClientProxy, EventPattern, Payload } from '@nestjs/microservices';
import { routes, services } from '@libs/api';
import { OrderResponse } from '@libs/api/routes/exchange/response/order.response';
import { AddOrderDto } from '@libs/api/routes/exchange/dto/add-order.dto';
import { lastValueFrom } from 'rxjs';
import { ExchangeGateway } from './exchange.gateway';
import { TradeDataInterface } from '@exchange/exchange/trade-data';

@Controller()
@ApiTags('Exchange')
export class ExchangeController {
  constructor(
    @Inject(services.ExchangeService)
    private readonly client: ClientProxy,
    private readonly exchangeGateway: ExchangeGateway,
  ) {}

  @Post('exchange/order')
  public async addOrder(@Body() dto: AddOrderDto): Promise<OrderResponse> {
    return lastValueFrom(this.client.send(routes.exchange.addOrder, dto));
  }

  @EventPattern(routes.exchange.events.onNewOrder)
  public async onNewOrder(@Payload() data: OrderResponse): Promise<void> {
    this.exchangeGateway.onNewOrder$.next(data);
  }

  @EventPattern(routes.exchange.events.onTrade)
  public async onTrade(@Payload() data: TradeDataInterface): Promise<void> {
    this.exchangeGateway.onTrade$.next(data);
  }
}
