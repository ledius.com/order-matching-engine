import { Module } from '@nestjs/common';
import { ExchangeController } from './exchange.controller';
import { ExchangeGateway } from './exchange.gateway';

@Module({
  controllers: [ExchangeController],
  providers: [ExchangeGateway],
})
export class ExchangeModule {}
