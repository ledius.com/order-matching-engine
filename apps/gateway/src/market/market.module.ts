import { Module } from '@nestjs/common';
import { MarketController } from '@gateway/market/market.controller';

@Module({
  controllers: [MarketController],
})
export class MarketModule {}
