import { Body, Controller, Get, Inject, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ClientProxy } from '@nestjs/microservices';
import { routes, services } from '@libs/api';
import { AddPriceDataDto } from '@market/price-data/dto/add-price-data.dto';
import { lastValueFrom } from 'rxjs';
import { PriceDataDocument } from '@market/price-data/dao/price-data';
import { AggregatePriceDataDto } from '@market/price-data/dto/aggregate-price-data.dto';
import { PriceDataAggregate } from '@market/price-data/controller/price-data-aggregate';

@Controller()
@ApiTags('Market')
export class MarketController {
  constructor(
    @Inject(services.MarketService)
    private readonly client: ClientProxy,
  ) {}

  @Post('market/price-data')
  public async addPrice(
    @Body() dto: AddPriceDataDto,
  ): Promise<PriceDataDocument> {
    return await lastValueFrom(
      this.client.send(routes.market.priceData.add, dto),
    );
  }

  @Get('market/price-data')
  public async aggregate(
    @Query() dto: AggregatePriceDataDto,
  ): Promise<PriceDataAggregate[]> {
    return lastValueFrom(
      this.client.send(routes.market.priceData.aggregate, dto),
    );
  }
}
